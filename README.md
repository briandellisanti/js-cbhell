An experiment comparing traditional synchronous, "linear" code and
asynchronous, callback-based code. 'linear.js' contains a synchronous API and
an example that uses the API. 'callback.js' contains a callback-based version
of the same API. The goal is to fill in an implementation of the example using
the callback-based API and produces identical output.

