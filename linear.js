var Connection = function () {
};
Connection.prototype.begin = function() {
  console.log('begin');
};
Connection.prototype.query = function(sql, params) {
  console.log('query: ' + sql + ': ' + JSON.stringify(params));
};
Connection.prototype.commit = function() {
  console.log('commit');
};
Connection.prototype.end = function() {
  console.log('end');
};
Connection.prototype.rollback = function() {
  console.log('rollback');
};

var i, conn = new Connection();
conn.begin();
try {
  for (i = 1; i <= 1024; i++) {
    if (i % 100 === 0) {
      conn.commit();
      conn.begin();
    }
    conn.query('insert into t set v=?', [i]);
  }
  conn.commit();
} catch (e) {
  conn.rollback();
} finally {
  conn.end();
}
