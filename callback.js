var Connection = function() {
};
// begin(function(err))
Connection.prototype.begin = function(fn) {
  setTimeout(function() {
    console.log('begin');
    fn();
  }, 50);
};
// query(string, array, function(err))
Connection.prototype.query = function(sql, params, fn) {
  setTimeout(function() {
    console.log('query: ' + sql + ': ' + JSON.stringify(params));
    fn();
  }, 1);
};
// commit(function(err))
Connection.prototype.commit = function(fn) {
  setTimeout(function() {
    console.log('commit');
    fn();
  }, 50);
};
// rollback(function(err))
Connection.prototype.rollback = function(fn) {
  setTimeout(function() {
    console.log('rollback');
    fn();
  }, 50);
};
Connection.prototype.end = function() {
  console.log('end');
};

