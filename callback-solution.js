// One possible solution

var Connection = function() {
};
// begin(function(err))
Connection.prototype.begin = function(fn) {
  setTimeout(function() {
    console.log('begin');
    fn();
  }, 50);
};
// query(string, array, function(err))
Connection.prototype.query = function(sql, params, fn) {
  setTimeout(function() {
    console.log('query: ' + sql + ': ' + JSON.stringify(params));
    fn();
  }, 1);
};
// commit(function(err))
Connection.prototype.commit = function(fn) {
  setTimeout(function() {
    console.log('commit');
    fn();
  }, 50);
};
// rollback(function(err))
Connection.prototype.rollback = function(fn) {
  setTimeout(function() {
    console.log('rollback');
    fn();
  }, 50);
};
Connection.prototype.end = function() {
  console.log('end');
};

var conn = new Connection();
var i, qcont, econt, body;

i = 1;

qcont = function(err) {
  if (!err) {
    i++;
  }
  body(err);
};

econt = function(err) {
  conn.rollback(function(err2) {
    conn.end();
    if (err2) {
      throw err2;
    }
    throw err;
  });
};

body = function(err) {
  if (err) {
    econt(err);
    return;
  }

  if (i > 1024) {
    conn.commit(function(err) {
      if (err) {
        econt(err);
        return;
      }
      conn.end();
    });
    return;
  }

  if (i % 100 === 0) {
    conn.commit(function(err) {
      if (err) {
        econt(err);
        return;
      }
      conn.begin(function(err) {
        if (err) {
          econt(err);
          return;
        }
        conn.query('insert into t set v=?', [i], qcont);
      });
    });
    return;
  }

  conn.query('insert into t set v=?', [i], qcont);
};

conn.begin(body);
